#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Arduino.h"

#include "led-effect.hpp"
#include "led-fader.hpp"

static uint8_t s_callback_count = 0;
static char s_message_buffer[128];

class LEDFaderTest : public CppUnit::TestFixture {

    CPPUNIT_TEST_SUITE(LEDFaderTest);

    CPPUNIT_TEST(testLEDFaderZeroAfterDirectionUpStart);
    CPPUNIT_TEST(testLEDFaderMaxAfterDirectionDownStart);
    CPPUNIT_TEST(testLEDFaderMaxAfterDirectionDownStartWithDifferentRGBValues);
    CPPUNIT_TEST(testLEDFaderLinearlyIncreasingUpdates);
    CPPUNIT_TEST(testLEDFaderLinearlyDecreasingUpdates);
    CPPUNIT_TEST(testLEDFaderSquaredAdjustIncreasingUpdates);

    CPPUNIT_TEST_SUITE_END();

    void testLEDFaderZeroAfterDirectionUpStart()
    {
        // Setup
        uint8_t actual[20][3];
        memset(actual, 0xFF, 60);

        uint8_t expected[20][3];

        LEDFader<uint8_t> s_fader = LEDFader<uint8_t>(actual, 20, LEDFaderType_Linear);
        s_fader.start(true, 255, 128, 64, 256);
        memset(expected, 0x00, 60);
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
    }

    void testLEDFaderMaxAfterDirectionDownStartWithDifferentRGBValues()
    {
        // Setup
        uint8_t actual[20][3];
        memset(actual, 0xFF, 60);

        uint8_t expected[20][3];

        LEDFader<uint8_t> s_fader = LEDFader<uint8_t>(actual, 20, LEDFaderType_Linear);
        s_fader.start(false, 50, 100, 200, 256);
        for (uint8_t i = 0; i < 20; i++)
        {
            expected[i][0] = 50;
            expected[i][1] = 100;
            expected[i][2] = 200;
        }
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
    }

    void testLEDFaderMaxAfterDirectionDownStart()
    {
        // Setup
        uint8_t actual[20][3];
        memset(actual, 0xFF, 60);

        uint8_t expected[20][3];

        LEDFader<uint8_t> s_fader = LEDFader<uint8_t>(actual, 20, LEDFaderType_Linear);
        s_fader.start(false, 255, 255, 255, 256);
        memset(expected, 255, 60);
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
    }


    void testLEDFaderLinearlyIncreasingUpdates()
    {
        // Setup
        uint8_t actual[20][3];
        memset(actual, 0xFF, 60);

        uint8_t expected_single_led[3] = {0, 0, 0};
        uint8_t expected[20][3];

        LEDFader<uint8_t> s_fader = LEDFader<uint8_t>(actual, 20, LEDFaderType_Linear);
        s_fader.start(true, 255, 128, 64, 256);

        for (uint16_t i = 0; i<256; i++)
        {
            CPPUNIT_ASSERT_EQUAL(i, s_fader.current_step());
            CPPUNIT_ASSERT(s_fader.update());
            expected_single_led[0] = (i * 255) / 256;
            expected_single_led[1] = (i * 128) / 256;
            expected_single_led[2] = (i * 64) / 256;
            for(uint8_t j=0; j<20; j++) {memcpy(&expected[j], expected_single_led, 3);}
            sprintf(s_message_buffer,"%d: %d, %d, %d != %d, %d, %d", i,
                (int)expected_single_led[0], (int)expected_single_led[1], (int)expected_single_led[2],
                (int)actual[0][0], (int)actual[0][1], (int)actual[0][2]
            );
            CPPUNIT_ASSERT_EQUAL_MESSAGE(s_message_buffer, 0, memcmp(actual, expected, 60));
        }

        CPPUNIT_ASSERT(!s_fader.update());

        expected_single_led[0] = 255;
        expected_single_led[1] = 128;
        expected_single_led[2] = 64;
        for(uint8_t j=0; j<20; j++) {memcpy(&expected[j], expected_single_led, 3);}
        sprintf(s_message_buffer,"255: %d, %d, %d != %d, %d, %d",
            (int)expected_single_led[0], (int)expected_single_led[1], (int)expected_single_led[2],
            (int)actual[0][0], (int)actual[0][1], (int)actual[0][2]
        );
        CPPUNIT_ASSERT_EQUAL_MESSAGE(s_message_buffer, 0, memcmp(actual, expected, 60));
    }

    void testLEDFaderLinearlyDecreasingUpdates()
    {
        // Setup
        uint8_t actual[20][3];
        memset(actual, 0xFF, 60);

        uint8_t expected_single_led[3] = {0, 0, 0};
        uint8_t expected[20][3];

        LEDFader<uint8_t> s_fader = LEDFader<uint8_t>(actual, 20, LEDFaderType_Linear);
        s_fader.start(false, 255, 128, 64, 256);

        for (uint16_t i = 0; i<256; i++)
        {
            CPPUNIT_ASSERT(s_fader.update());
            expected_single_led[0] = ((256-i) * 255) / 256;
            expected_single_led[1] = ((256-i) * 128) / 256;
            expected_single_led[2] = ((256-i) * 64) / 256;
            for(uint8_t j=0; j<20; j++) {memcpy(&expected[j], expected_single_led, 3);}
            sprintf(s_message_buffer,"%d: %d, %d, %d != %d, %d, %d", i,
                (int)expected_single_led[0], (int)expected_single_led[1], (int)expected_single_led[2],
                (int)actual[0][0], (int)actual[0][1], (int)actual[0][2]
            );
            CPPUNIT_ASSERT_EQUAL_MESSAGE(s_message_buffer, 0, memcmp(actual, expected, 60));
        }
        CPPUNIT_ASSERT(!s_fader.update());
        memset(expected, 0, 60);
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
    }

    void testLEDFaderSquaredAdjustIncreasingUpdates()
    {
        // Setup
        uint8_t actual[20][3];
        memset(actual, 0xFF, 60);

        uint8_t expected[20][3];

        LEDFader<uint8_t> s_fader = LEDFader<uint8_t>(actual, 20, LEDFaderType_SquaredAdjust);
        s_fader.start(true, 255, 255, 255, 256);

        for (uint16_t i = 0; i<64; i++)
        {
            CPPUNIT_ASSERT(s_fader.update());
        }

        memset(expected, (63*63*255)/(256*256), 60);
        sprintf(s_message_buffer, "%d,%d,%d != %d,%d,%d",
            (int)expected[0][0], (int)expected[0][1], (int)expected[0][2],
            (int)actual[0][0], (int)actual[0][1], (int)actual[0][2]);
        CPPUNIT_ASSERT_EQUAL_MESSAGE(s_message_buffer, 0, memcmp(actual, expected, 60));

        for (uint16_t i = 0; i<64; i++)
        {
            CPPUNIT_ASSERT(s_fader.update());
        }

        memset(expected, (127*127*255)/(256*256), 60);
        sprintf(s_message_buffer, "%d,%d,%d != %d,%d,%d",
            (int)expected[0][0], (int)expected[0][1], (int)expected[0][2],
            (int)actual[0][0], (int)actual[0][1], (int)actual[0][2]);
        CPPUNIT_ASSERT_EQUAL_MESSAGE(s_message_buffer, 0, memcmp(actual, expected, 60));

        for (uint16_t i = 0; i<64; i++)
        {
            CPPUNIT_ASSERT(s_fader.update());
        }

        memset(expected, (191*191*255)/(256*256), 60);
        sprintf(s_message_buffer, "%d,%d,%d != %d,%d,%d",
            (int)expected[0][0], (int)expected[0][1], (int)expected[0][2],
            (int)actual[0][0], (int)actual[0][1], (int)actual[0][2]);
        CPPUNIT_ASSERT_EQUAL_MESSAGE(s_message_buffer, 0, memcmp(actual, expected, 60));

        for (uint16_t i = 0; i<64; i++)
        {
            CPPUNIT_ASSERT(s_fader.update());
        }

        memset(expected, (255*255*255)/(256*256), 60);
        sprintf(s_message_buffer, "%d,%d,%d != %d,%d,%d",
            (int)expected[0][0], (int)expected[0][1], (int)expected[0][2],
            (int)actual[0][0], (int)actual[0][1], (int)actual[0][2]);
        CPPUNIT_ASSERT_EQUAL_MESSAGE(s_message_buffer, 0, memcmp(actual, expected, 60));

        CPPUNIT_ASSERT(!s_fader.update());

        memset(expected, 255, 60);
        sprintf(s_message_buffer, "%d,%d,%d != %d,%d,%d",
            (int)expected[0][0], (int)expected[0][1], (int)expected[0][2],
            (int)actual[0][0], (int)actual[0][1], (int)actual[0][2]);
        CPPUNIT_ASSERT_EQUAL_MESSAGE(s_message_buffer, 0, memcmp(actual, expected, 60));
    }

public:
    void setUp()
    {
        s_callback_count = 0;
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(LEDFaderTest);

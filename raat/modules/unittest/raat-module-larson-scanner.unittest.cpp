#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Arduino.h"

#include "led-effect.hpp"
#include "larson-scanner.hpp"

#define N_LARSON_LEDS 5

static uint8_t s_callback_count = 0;
static char s_message_buffer[128];

void larson_value_callback(uint8_t index, uint8_t& r_multiplier, uint8_t& r_divisor, uint8_t& r_offset)
{
    uint8_t multiplier = index+1;
    r_multiplier = multiplier;
    r_divisor = (N_LARSON_LEDS + 1) / 2;
    r_offset = 0;
    s_callback_count++;
}

class LarsonScannerTest : public CppUnit::TestFixture {

    int32_t numbers[16];

    CPPUNIT_TEST_SUITE(LarsonScannerTest);

    CPPUNIT_TEST(testLarsonScannerStart);
    CPPUNIT_TEST(testLarsonScannerMoveOnce);
    CPPUNIT_TEST(testLarsonScannerMoveToEnd);
    CPPUNIT_TEST(testLarsonScannerBounceAtRight);
    CPPUNIT_TEST(testLarsonScannerReturnToLeftSingleRun);
    CPPUNIT_TEST(testLarsonScannerReturnToLeftContinuousRun);
    CPPUNIT_TEST(testLarsonScannerBounceAtLeft);
    CPPUNIT_TEST(testLarsonScannerRunOnce);
    CPPUNIT_TEST(testLarsonScannerRunSeveralTimes);
    CPPUNIT_TEST(testLarsonScannerRunForever);

    CPPUNIT_TEST_SUITE_END();

    void testLarsonScannerStart()
    {
        // Setup
        uint8_t actual[20][3] = {0xFF};
        uint8_t expected[20][3] = {
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF, 0xFF, 0xFF},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0, 0, 0}
        };
        LarsonScanner<uint8_t> s_larson = LarsonScanner<uint8_t>((uint8_t*)actual, 20, 5, 10, larson_value_callback);

        // Action
        s_larson.start(255,255,255);

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
    }

    void testLarsonScannerMoveOnce()
    {
        // Setup
        uint8_t actual[20][3] = {0xFF};
        uint8_t expected[20][3] = {
            {0, 0, 0},
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF, 0xFF, 0xFF},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0, 0, 0}
        };
        LarsonScanner<uint8_t> s_larson = LarsonScanner<uint8_t>((uint8_t*)actual, 20, 5, 10, larson_value_callback);
        s_larson.start(255,255,255);

        // Action
        millis_set(10);
        s_larson.update();

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
    }

    void testLarsonScannerMoveToEnd()
    {
        // Setup
        uint8_t actual[20][3] = {0xFF};
        uint8_t expected[20][3] = {
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF, 0xFF, 0xFF},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
        };
        LarsonScanner<uint8_t> s_larson = LarsonScanner<uint8_t>((uint8_t*)actual, 20, 5, 10, larson_value_callback);
        s_larson.start(255,255,255);

        // Action
        for (uint8_t i=0; i<15; i++)
        {
            millis_set((i+1)*10);
            s_larson.update();
        }

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
    }

    void testLarsonScannerBounceAtRight()
    {
        // Setup
        uint8_t actual[20][3] = {0xFF};
        uint8_t expected[20][3] = {
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF, 0xFF, 0xFF},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0, 0, 0},
        };
        LarsonScanner<uint8_t> s_larson = LarsonScanner<uint8_t>((uint8_t*)actual, 20, 5, 10, larson_value_callback);
        s_larson.start(255,255,255);

        // Action
        for (uint8_t i=0; i<16; i++)
        {
            millis_set((i+1)*10);
            s_larson.update();
        }

        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
    }

    void testLarsonScannerReturnToLeftSingleRun()
    {
        // Setup
        uint8_t actual[20][3] = {0xFF};
        uint8_t expected[20][3] = {
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF, 0xFF, 0xFF},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0, 0, 0}
        };
        LarsonScanner<uint8_t> s_larson = LarsonScanner<uint8_t>((uint8_t*)actual, 20, 5, 10, larson_value_callback);
        s_larson.start(255,255,255, 1);

        // Action
        for (uint8_t i=0; i<30; i++)
        {
            millis_set((i+1)*10);
            s_larson.update();
        }
        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
    }

    void testLarsonScannerReturnToLeftContinuousRun()
    {
        // Setup
        uint8_t actual[20][3] = {0xFF};
        uint8_t expected[20][3] = {
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF, 0xFF, 0xFF},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0, 0, 0}
        };
        LarsonScanner<uint8_t> s_larson = LarsonScanner<uint8_t>((uint8_t*)actual, 20, 5, 10, larson_value_callback);
        s_larson.start(255,255,255);

        // Action
        for (uint8_t i=0; i<30; i++)
        {
            millis_set((i+1)*10);
            s_larson.update();
        }
        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
    }

    void testLarsonScannerBounceAtLeft()
    {
        // Setup
        uint8_t actual[20][3] = {0xFF};
        uint8_t expected[20][3] = {
            {0, 0, 0},
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF, 0xFF, 0xFF},
            {0xFF*2/3, 0xFF*2/3, 0xFF*2/3},
            {0xFF*1/3, 0xFF*1/3, 0xFF*1/3},
            {0, 0, 0}
        };
        LarsonScanner<uint8_t> s_larson = LarsonScanner<uint8_t>((uint8_t*)actual, 20, 5, 10, larson_value_callback);
        s_larson.start(255,255,255);

        // Action
        for (uint8_t i=0; i<31; i++)
        {
            millis_set((i+1)*10);
            s_larson.update();
        }

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
    }

    void testLarsonScannerRunOnce()
    {
        // Setup
        uint8_t i;
        uint8_t actual[20][3] = {0xFF};
        uint8_t expected[20][3] = {0};
        LarsonScanner<uint8_t> s_larson = LarsonScanner<uint8_t>((uint8_t*)actual, 20, 5, 10, larson_value_callback);
        s_larson.start(255,255,255, 1);

        // Action
        bool update_sentinel = true;
        for (i=0; i<30; i++)
        {
            millis_set((i+1)*10);
            update_sentinel &= s_larson.update();
            CPPUNIT_ASSERT(update_sentinel);
        }
        millis_set((i+1)*10);

        // Test
        CPPUNIT_ASSERT(update_sentinel);
        CPPUNIT_ASSERT(!s_larson.update());
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
        CPPUNIT_ASSERT(s_callback_count > 0);
    }

    void testLarsonScannerRunSeveralTimes()
    {
        // Setup
        uint8_t i;
        uint8_t actual[20][3] = {0xFF};
        uint8_t expected[20][3] = {0};
        LarsonScanner<uint8_t> s_larson = LarsonScanner<uint8_t>((uint8_t*)actual, 20, 5, 10, larson_value_callback);
        s_larson.start(255,255,255, 3);

        // Action
        bool update_sentinel = true;
        for (i=0; i<90; i++)
        {
            millis_set((i+1)*10);
            update_sentinel &= s_larson.update();
        }
        millis_set((i+1)*10);

        // Test
        CPPUNIT_ASSERT(update_sentinel);
        CPPUNIT_ASSERT(!s_larson.update());

        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 60));
        CPPUNIT_ASSERT(s_callback_count > 0);
    }

    void testLarsonScannerRunForever()
    {
        // Setup
        uint8_t actual[20][3] = {0xFF};
        LarsonScanner<uint8_t> s_larson = LarsonScanner<uint8_t>((uint8_t*)actual, 20, 5, 10, larson_value_callback);
        s_larson.start(255,255,255);

        // Action
        bool update_sentinel = true;
        for (uint32_t i=0; i<999999; i++)
        {
            millis_set((i+1)*10);
            update_sentinel &= s_larson.update();
        }

        // Test
        CPPUNIT_ASSERT(update_sentinel);
        CPPUNIT_ASSERT(s_callback_count > 0);
    }

public:
    void setUp()
    {
        s_callback_count = 0;
        millis_set(0);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(LarsonScannerTest);

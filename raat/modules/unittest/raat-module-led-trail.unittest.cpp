#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Arduino.h"

#include "led-effect.hpp"
#include "led-trail.hpp"

#define N_TRAIL_LEDS 5
#define N_STRIP_LEDS 20
#define SENTINEL_VALUE 0x55

static uint8_t s_callback_count = 0;
static LEDTrail<uint8_t> * sp_complete_callback_ref = NULL;
//static char s_message_buffer[128];

static void led_trail_value_callback(const LEDTrail<uint8_t>& r_trail, uint8_t index, uint8_t (&r_multipliers)[3], uint8_t (&r_divisors)[3], uint8_t (&r_offsets)[3])
{
    (void)r_trail;
    memset(r_multipliers, index+1, 3);
    memset(r_divisors, N_TRAIL_LEDS, 3);
    memset(r_offsets, 0, 3);
    s_callback_count++;
}

static void led_trail_complete_callback(LEDTrail<uint8_t>& trail)
{
    sp_complete_callback_ref = &trail;
}

static void set_expected_value(uint8_t expected[N_STRIP_LEDS][3], uint8_t index, uint8_t sentinel_value)
{
    expected[index][0] = expected[index][1] = expected[index][2] = sentinel_value;
}

class LEDTrailTest : public CppUnit::TestFixture {

    uint8_t actual[N_STRIP_LEDS+1][3];

    CPPUNIT_TEST_SUITE(LEDTrailTest);

    CPPUNIT_TEST(testLEDTrailStart);
    CPPUNIT_TEST(testLEDTrailStartAtIndex0);
    CPPUNIT_TEST(testLEDTrailStartAtIndex1);
    CPPUNIT_TEST(testLEDTrailStartAtIndex4);
    CPPUNIT_TEST(testLEDTrailMoveOnce);
    CPPUNIT_TEST(testLEDTrailMoveToEndOfStrip);
    CPPUNIT_TEST(testLEDTrailMovePastEndOfStrip);
    CPPUNIT_TEST(testLEDTrailMoveToOneLEDLeftOn);
    CPPUNIT_TEST(testLEDTrailMoveCompletelyOffStrip);
    //CPPUNIT_TEST(testLEDTrailRunOnce);
    //CPPUNIT_TEST(testLEDTrailRunSeveralTimes);
    //CPPUNIT_TEST(testLEDTrailRunForever);

    CPPUNIT_TEST_SUITE_END();

    void testLEDTrailStart()
    {
        // Setup
        uint8_t expected[21][3] = {0x0};
        set_expected_value(expected, N_STRIP_LEDS, SENTINEL_VALUE);

        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);

        // Action
        s_led_trail.start(255,255,255);

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 63));
    }

    void testLEDTrailStartAtIndex0()
    {
        // Setup
        uint8_t expected[21][3] = {0x0};
        set_expected_value(expected, 0, 0xFF);
        set_expected_value(expected, N_STRIP_LEDS, SENTINEL_VALUE);
        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);

        // Action
        s_led_trail.start(255,255,255, CONTINUOUS_EFFECT, 0);

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 63));
    }

    void testLEDTrailStartAtIndex1()
    {
        // Setup
        uint8_t expected[21][3] = {0x0};
        set_expected_value(expected, 0, 0xFF*4/5);
        set_expected_value(expected, 1, 0xFF);
        set_expected_value(expected, N_STRIP_LEDS, SENTINEL_VALUE);
        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);

        // Action
        s_led_trail.start(255,255,255, CONTINUOUS_EFFECT, 1);

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 63));
    }

    void testLEDTrailStartAtIndex4()
    {
        // Setup
        uint8_t expected[21][3] = {0x0};
        set_expected_value(expected, 0, 0xFF*1/5);
        set_expected_value(expected, 1, 0xFF*2/5);
        set_expected_value(expected, 2, 0xFF*3/5);
        set_expected_value(expected, 3, 0xFF*4/5);
        set_expected_value(expected, 4, 0xFF);
        set_expected_value(expected, N_STRIP_LEDS, SENTINEL_VALUE);
        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);

        // Action
        s_led_trail.start(255,255,255, CONTINUOUS_EFFECT, 4);

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 63));
    }

    void testLEDTrailMoveOnce()
    {
        // Setup
        uint8_t expected[21][3] = {0x0};
        set_expected_value(expected, 0, 0xFF);
        set_expected_value(expected, N_STRIP_LEDS, SENTINEL_VALUE);
        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);
        s_led_trail.start(255,255,255);

        // Action
        millis_set(10);
        s_led_trail.update();

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 63));
    }

    void testLEDTrailMoveToEndOfStrip()
    {
        // Setup
        uint8_t expected[21][3] = {0x0};
        set_expected_value(expected, 15, 0xFF*1/5);
        set_expected_value(expected, 16, 0xFF*2/5);
        set_expected_value(expected, 17, 0xFF*3/5);
        set_expected_value(expected, 18, 0xFF*4/5);
        set_expected_value(expected, 19, 0xFF);
        set_expected_value(expected, N_STRIP_LEDS, SENTINEL_VALUE);

        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);
        s_led_trail.start(255,255,255);

        // Action
        for (uint8_t i=0; i<N_STRIP_LEDS; i++)
        {
            millis_set((i+1)*10);
            s_led_trail.update();
        }

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 63));
        CPPUNIT_ASSERT(NULL == sp_complete_callback_ref);
    }

    void testLEDTrailMovePastEndOfStrip()
    {
        // Setup
        uint8_t expected[21][3] = {0x0};
        set_expected_value(expected, 16, 0xFF*1/5);
        set_expected_value(expected, 17, 0xFF*2/5);
        set_expected_value(expected, 18, 0xFF*3/5);
        set_expected_value(expected, 19, 0xFF*4/5);
        set_expected_value(expected, N_STRIP_LEDS, SENTINEL_VALUE);
        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);
        s_led_trail.start(255,255,255);

        // Action
        for (uint8_t i=0; i<21; i++)
        {
            millis_set((i+1)*10);
            s_led_trail.update();
        }

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 63));
        CPPUNIT_ASSERT(NULL == sp_complete_callback_ref);
    }

    void testLEDTrailMoveToOneLEDLeftOn()
    {
        // Setup
        uint8_t expected[21][3] = {0x0};
        set_expected_value(expected, 19, 0xFF*1/5);
        set_expected_value(expected, N_STRIP_LEDS, SENTINEL_VALUE);
        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);
        s_led_trail.start(255,255,255);

        // Action
        for (uint8_t i=0; i<24; i++)
        {
            millis_set((i+1)*10);
            s_led_trail.update();
        }

        // Test
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 63));
        CPPUNIT_ASSERT(NULL == sp_complete_callback_ref);
    }

    void testLEDTrailMoveCompletelyOffStrip()
    {
        // Setup
        uint8_t expected[21][3] = {0x0};
        set_expected_value(expected, N_STRIP_LEDS, SENTINEL_VALUE);
        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);
        s_led_trail.start(255,255,255);

        // Action
        uint8_t i=0;
        bool update_sentinel = true;
        for (; i<24; i++)
        {
            millis_set((i+1)*10);
            update_sentinel &= s_led_trail.update();
        }

        millis_set((i+1)*10);

        // Test
        CPPUNIT_ASSERT(update_sentinel);
        CPPUNIT_ASSERT(!s_led_trail.update());
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 63));
        CPPUNIT_ASSERT(&s_led_trail == sp_complete_callback_ref);
    }

    /*void testLEDTrailRunOnce()
    {
        // Setup
        uint8_t i;
        uint8_t expected[21][3] = {0};
        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);
        s_led_trail.start(255,255,255, 1);

        // Action
        bool update_sentinel = true;
        for (i=0; i<N_STRIP_LEDS; i++)
        {
            millis_set((i+1)*10);
            update_sentinel &= s_led_trail.update();
            CPPUNIT_ASSERT(update_sentinel);
        }
        millis_set((i+1)*10);

        // Test
        CPPUNIT_ASSERT(update_sentinel);
        CPPUNIT_ASSERT(!s_led_trail.update());
        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 63));
        CPPUNIT_ASSERT(s_callback_count > 0);
    }

    void testLEDTrailRunSeveralTimes()
    {
        // Setup
        uint8_t i;
        uint8_t expected[21][3] = {0};
        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);
        s_led_trail.start(255,255,255, 3);

        // Action
        bool update_sentinel = true;
        for (i=0; i<90; i++)
        {
            millis_set((i+1)*10);
            update_sentinel &= s_led_trail.update();
        }
        millis_set((i+1)*10);

        // Test
        CPPUNIT_ASSERT(update_sentinel);
        CPPUNIT_ASSERT(!s_led_trail.update());

        CPPUNIT_ASSERT_EQUAL(0, memcmp(actual, expected, 63));
        CPPUNIT_ASSERT(s_callback_count > 0);
    }

    void testLEDTrailRunForever()
    {
        // Setup
        LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(actual, N_STRIP_LEDS, 5, 10, led_trail_value_callback, led_trail_complete_callback);
        s_led_trail.start(255,255,255);

        // Action
        bool update_sentinel = true;
        for (uint32_t i=0; i<999999; i++)
        {
            millis_set((i+1)*10);
            update_sentinel &= s_led_trail.update();
        }

        // Test
        CPPUNIT_ASSERT(update_sentinel);
        CPPUNIT_ASSERT(s_callback_count > 0);
    }*/

public:
    void setUp()
    {
        memset(actual, SENTINEL_VALUE, sizeof(actual));
        s_callback_count = 0;
        sp_complete_callback_ref = NULL;
        millis_set(0);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(LEDTrailTest);

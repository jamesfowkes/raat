#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include "raat.hpp"
#include "raat-oneshot-timer.hpp"

#include "blinker.hpp"

static bool sb_last_state = false;
static int sb_call_count = 0;

static void on_blink_change_cb(bool state)
{
    sb_last_state = state;
    sb_call_count++;
}

class BlinkerTest : public CppUnit::TestFixture { 

    CPPUNIT_TEST_SUITE(BlinkerTest);

    CPPUNIT_TEST(testBlinker_BlinkOnceEqualPeriods);
    CPPUNIT_TEST(testBlinker_BlinkOnceUnequalPeriods);
    CPPUNIT_TEST(testBlinker_BlinkTwice);
    CPPUNIT_TEST(testBlinker_BlinkThreeTimes);
    CPPUNIT_TEST(testBlinker_OnOffBehaviour);
    CPPUNIT_TEST(testBlinker_OffOnBehaviourWithOnEndState);
    CPPUNIT_TEST(testBlinker_OffOnBehaviourWithOffEndState);
    CPPUNIT_TEST(testBlinker_OnOffBehaviourWithOnEndState);
    CPPUNIT_TEST(testBlinker_OnOffBehaviourWithOffEndState);
    CPPUNIT_TEST(testBlinker_BlinkForever);
    CPPUNIT_TEST(testBlinker_RunningReturnsCorrectValue);

    CPPUNIT_TEST_SUITE_END();

    void testBlinker_BlinkOnceEqualPeriods()
    {
        Blinker blinker = Blinker(500, 500, on_blink_change_cb);
        blinker.start(1);

        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(1, sb_call_count);

        millis_set(500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(2, sb_call_count);

        millis_set(1000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(3, sb_call_count);

        millis_set(1500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(3, sb_call_count);
    }

    void testBlinker_BlinkOnceUnequalPeriods()
    {
        Blinker blinker = Blinker(500, 100, on_blink_change_cb);
        blinker.start(1);

        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(1, sb_call_count);

        millis_set(100);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(2, sb_call_count);

        millis_set(600);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(3, sb_call_count);

        millis_set(700);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(3, sb_call_count);
    }

    void testBlinker_BlinkTwice()
    {
        Blinker blinker = Blinker(500, 500, on_blink_change_cb);
        blinker.start(2);

        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(1, sb_call_count);

        millis_set(500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(2, sb_call_count);

        millis_set(1000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(3, sb_call_count);

        millis_set(1500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(4, sb_call_count);

        millis_set(2000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);

        millis_set(2500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);
    }

    void testBlinker_BlinkThreeTimes()
    {
        Blinker blinker = Blinker(500, 500, on_blink_change_cb);
        blinker.start(3);

        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(1, sb_call_count);

        millis_set(500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(2, sb_call_count);

        millis_set(1000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(3, sb_call_count);

        millis_set(1500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(4, sb_call_count);

        millis_set(2000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);

        millis_set(2500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(6, sb_call_count);

        millis_set(3000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(7, sb_call_count);

        millis_set(3500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(7, sb_call_count);
    }

    void testBlinker_OnOffBehaviour()
    {
        Blinker blinker = Blinker(500, 500, on_blink_change_cb);
        blinker.setBehaviour(eBlinkerBehaviour_OnOff);
        blinker.start(2);

        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(1, sb_call_count);

        millis_set(500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(2, sb_call_count);

        millis_set(1000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(3, sb_call_count);

        millis_set(1500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(4, sb_call_count);

        millis_set(2000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);

        millis_set(2500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);
    }

    void testBlinker_OffOnBehaviourWithOnEndState()
    {
        Blinker blinker = Blinker(500, 500, on_blink_change_cb);
        blinker.setBehaviour(eBlinkerBehaviour_OffOn);
        blinker.setEndState(eBlinkerEndState_On);
        blinker.start(2);

        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(1, sb_call_count);

        millis_set(500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(2, sb_call_count);

        millis_set(1000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(3, sb_call_count);

        millis_set(1500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(4, sb_call_count);

        millis_set(2000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);

        millis_set(2500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);
    }

    void testBlinker_OffOnBehaviourWithOffEndState()
    {
        Blinker blinker = Blinker(500, 500, on_blink_change_cb);
        blinker.setBehaviour(eBlinkerBehaviour_OffOn);
        blinker.setEndState(eBlinkerEndState_Off);
        blinker.start(2);

        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(1, sb_call_count);

        millis_set(500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(2, sb_call_count);

        millis_set(1000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(3, sb_call_count);

        millis_set(1500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(4, sb_call_count);

        millis_set(2000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);

        millis_set(2500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);
    }

    void testBlinker_OnOffBehaviourWithOnEndState()
    {
        Blinker blinker = Blinker(500, 500, on_blink_change_cb);
        blinker.setBehaviour(eBlinkerBehaviour_OnOff);
        blinker.setEndState(eBlinkerEndState_On);
        blinker.start(2);

        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(1, sb_call_count);

        millis_set(500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(2, sb_call_count);

        millis_set(1000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(3, sb_call_count);

        millis_set(1500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(4, sb_call_count);

        millis_set(2000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);

        millis_set(2500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);
    }

    void testBlinker_OnOffBehaviourWithOffEndState()
    {
        Blinker blinker = Blinker(500, 500, on_blink_change_cb);
        blinker.setBehaviour(eBlinkerBehaviour_OnOff);
        blinker.setEndState(eBlinkerEndState_Off);
        blinker.start(2);

        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(1, sb_call_count);

        millis_set(500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(2, sb_call_count);

        millis_set(1000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(3, sb_call_count);

        millis_set(1500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(4, sb_call_count);

        millis_set(2000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);

        millis_set(2500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(5, sb_call_count);
    }

    void testBlinker_BlinkForever()
    {
        Blinker blinker = Blinker(500, 500, on_blink_change_cb);
        blinker.start(BLINK_FOREVER);

        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(1, sb_call_count);

        millis_set(500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(2, sb_call_count);

        for (uint16_t i = 1000; i < 20000; i++)
        {
            millis_set(i);
            blinker.update(); 
        }

        CPPUNIT_ASSERT_EQUAL(true, sb_last_state);
        CPPUNIT_ASSERT_EQUAL(40, sb_call_count);
    }

    void testBlinker_RunningReturnsCorrectValue()
    {
        Blinker blinker = Blinker(500, 500, on_blink_change_cb);
        blinker.start(3);

        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, blinker.running());

        millis_set(500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, blinker.running());

        millis_set(1000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, blinker.running());

        millis_set(1500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, blinker.running());

        millis_set(2000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, blinker.running());

        millis_set(2500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(true, blinker.running());

        millis_set(3000);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, blinker.running());

        millis_set(3500);
        blinker.update();
        CPPUNIT_ASSERT_EQUAL(false, blinker.running());
    }

public:
    void setUp()
    {
        sb_last_state = false;
        sb_call_count = 0;
        millis_set(0);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(BlinkerTest);

#ifdef ARDUINO
#include <Arduino.h>
#ifdef AVR_PLATFORM
#include <avr/pgmspace.h>
#endif
#endif

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "raat.hpp"
#include "raat-oneshot-timer.hpp"

#include "blinker.hpp"

/* Blinker class */

/*
 * Blinker::Blinker
 */

Blinker::Blinker(uint16_t ontime, uint16_t offtime, blink_change_cb_fn callback) :
    m_ontime(ontime), m_offtime(offtime), m_state(false), m_callback(callback),
    m_timer(offtime), m_nblinks(BLINK_FOREVER), m_end_state(eBlinkerEndState_UseBehaviour),
    m_behaviour(eBlinkerBehaviour_OffOn)
{
}

Blinker::~Blinker() {}

void Blinker::start(uint16_t number_of_blinks)
{
    m_nblinks = number_of_blinks;
    m_state = (m_behaviour == eBlinkerBehaviour_OnOff);
    m_callback(m_state);
    m_timer.start(m_offtime);
}

bool Blinker::running()
{
    return m_nblinks > 0;
}

void Blinker::update(void)
{
    if (m_timer.check_and_reset())
    {
        // Invert the state
        m_state = !m_state;

        // If blinks are finite and done with this blink and decrement number of blinks
        if (m_nblinks != BLINK_FOREVER)
        {
            bool this_blink_finished = false;
            this_blink_finished |= ((m_behaviour == eBlinkerBehaviour_OnOff) && (m_state == true));
            this_blink_finished |= ((m_behaviour == eBlinkerBehaviour_OffOn) && (m_state == false));

            if (this_blink_finished)
            {
                m_nblinks--;
            }
        }

        // If still blinks to go, invoke callback then restart the timer with the new state
        if (m_nblinks)
        {
            m_callback(m_state);
            m_timer.start(m_state ? m_ontime : m_offtime);
        }
        else
        {
            switch (m_end_state)
            {
                case eBlinkerEndState_UseBehaviour:
                    m_callback(m_state);
                    break;  
                case eBlinkerEndState_On:
                    m_callback(true);
                    break;  
                case eBlinkerEndState_Off:
                    m_callback(false);
                    break;  
            }
        }
    }
}

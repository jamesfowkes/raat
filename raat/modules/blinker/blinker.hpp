#ifndef _BLINKER_H_
#define _BLINKER_H_

typedef void (*blink_change_cb_fn)(bool state);

static const uint16_t BLINK_FOREVER = 0xFFFF;

enum eBlinkerBehaviour
{
    eBlinkerBehaviour_OnOff,
    eBlinkerBehaviour_OffOn
};

enum eBlinkerEndState
{
    eBlinkerEndState_UseBehaviour,
    eBlinkerEndState_On,
    eBlinkerEndState_Off,
};

class Blinker
{
    public:
        Blinker(uint16_t ontime, uint16_t offtime, blink_change_cb_fn callback);
        ~Blinker();
        void start(uint16_t number_of_blinks);
        void update();
        void set_timings(uint16_t ontime, uint16_t offtime) { m_ontime = ontime; m_offtime = offtime; }
        void setBehaviour(enum eBlinkerBehaviour behaviour) { m_behaviour = behaviour; }
        void setEndState(enum eBlinkerEndState end_state) { m_end_state = end_state; }

        bool running();
    private:
        uint16_t m_ontime;
        uint16_t m_offtime;
        bool m_state;
        blink_change_cb_fn m_callback;
        RAATOneShotTimer m_timer;
        uint16_t m_nblinks;
        enum eBlinkerEndState m_end_state;
        enum eBlinkerBehaviour m_behaviour;
};

#endif

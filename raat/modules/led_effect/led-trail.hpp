#ifndef _RAAT_LED_TRAIL_H_
#define _RAAT_LED_TRAIL_H_

/* C/C++ Includes */

#include <stdint.h>

/* Module Includes */

#include "led-effect.hpp"

/* Defines, constants, typedefs */

static constexpr int LED_TRAIL_START_OFFSTRIP = -1;

/* Private Functions */

static uint32_t calculate_brightness(uint32_t base_value, uint32_t multiplier, uint32_t divisor, uint32_t offset, uint32_t max_value)
{
    uint32_t new_value = ((base_value * multiplier) / divisor) + offset;
    return new_value < max_value ? new_value : max_value;
}

template <class INT_TYPE>
class LEDTrail
{
public:
    LEDTrail(INT_TYPE (*dst)[3], uint8_t n_strip_leds, uint8_t n_trail_leds, uint32_t update_period,
        void (*pfn_value_getter)(const LEDTrail<INT_TYPE>& r_trail, uint8_t index, INT_TYPE (&r_multipliers)[3], INT_TYPE (&r_divisors)[3], INT_TYPE (&r_offsets)[3]),
        void (*pfn_complete_cb)(LEDTrail<INT_TYPE>& r_trail)
    );

    void setup();
    void reset();
    void tick();

    void print();
    void print(INT_TYPE * pleds);
    void print(INT_TYPE * pleds, uint8_t min_index, uint8_t max_index);

    void start(INT_TYPE r, INT_TYPE g, INT_TYPE b);
    void start(INT_TYPE r, INT_TYPE g, INT_TYPE b, int8_t number_of_runs, int8_t head_start_index=LED_TRAIL_START_OFFSTRIP);

    bool update();
private:

    void set_next();
    void clear_last_pixel();
    void write_led_array();

    RAATOneShotTimer m_timer;
    INT_TYPE (*mp_leds)[3];
    INT_TYPE (*mp_values)[3];
    uint8_t m_n_strip_leds;
    uint8_t m_n_trail_leds;
    int8_t m_tail_location_idx; // Can be negative if the tail is behind the start of the string
    int8_t m_head_start_index;
    int8_t m_runs;
    bool m_running;

    void (*m_pfn_value_getter)(const LEDTrail<INT_TYPE>& r_trail, uint8_t index, INT_TYPE (&r_multipliers)[3], INT_TYPE (&r_divisors)[3], INT_TYPE (&r_offsets)[3]);
    void (*m_pfn_complete_cb)(LEDTrail<INT_TYPE>& r_trail);
};

template <class INT_TYPE>
LEDTrail<INT_TYPE>::LEDTrail(INT_TYPE (*dst)[3], uint8_t n_strip_leds, uint8_t n_trail_leds, uint32_t update_period,
    void (*pfn_value_getter)(const LEDTrail<INT_TYPE>& r_trail, uint8_t index, INT_TYPE (&r_multipliers)[3], INT_TYPE (&r_divisors)[3], INT_TYPE (&r_offsets)[3]),
    void (*pfn_complete_cb)(LEDTrail<INT_TYPE>& r_trail)) :
    m_timer(update_period),
    mp_leds(dst), m_n_strip_leds(n_strip_leds), m_n_trail_leds(n_trail_leds),
    m_tail_location_idx(0),
    m_pfn_value_getter(pfn_value_getter),
    m_pfn_complete_cb(pfn_complete_cb)
{
    mp_values = (INT_TYPE(*)[3])malloc(n_trail_leds * 3 * sizeof(INT_TYPE));
}

#ifndef ARDUINO
template <class INT_TYPE>
void LEDTrail<INT_TYPE>::print()
{
    this->print(mp_leds);
}

template <class INT_TYPE>
void LEDTrail<INT_TYPE>::print(INT_TYPE * pleds)
{
    uint8_t min_index = m_tail_location_idx;
    if (min_index > 0)
    {
        min_index--;
    }

    uint8_t max_index = m_tail_location_idx;

    if (max_index < m_n_strip_leds)
    {
        max_index++;
    }

    this->print(pleds, min_index, max_index);
}

template <class INT_TYPE>
void LEDTrail<INT_TYPE>::print(INT_TYPE * pleds, uint8_t min_index, uint8_t max_index)
{
    std::cout << "LED values " << (int)min_index << "-" << (int)max_index << ":" << std::endl;
    for (uint8_t i=min_index; i<=max_index; i++)
    {
        std::cout << (int)pleds[i*3] << ","<< (int)pleds[i*3+1] << ","<< (int)pleds[i*3+2] << ",";
    }
    std::cout << std::endl;
}
#endif


template <class INT_TYPE>
void LEDTrail<INT_TYPE>::start(INT_TYPE r, INT_TYPE g, INT_TYPE b)
{
    this->start(r, g, b, CONTINUOUS_EFFECT, LED_TRAIL_START_OFFSTRIP);
}

template <class INT_TYPE>
void LEDTrail<INT_TYPE>::start(INT_TYPE r, INT_TYPE g, INT_TYPE b, int8_t number_of_runs, int8_t head_start_index)
{
    m_runs = number_of_runs;
    m_running = (number_of_runs > 0) || (number_of_runs == CONTINUOUS_EFFECT);
    m_tail_location_idx = (head_start_index - m_n_trail_leds) + 1;
    m_head_start_index = head_start_index;

    INT_TYPE divisors[3];
    INT_TYPE multipliers[3];
    INT_TYPE offsets[3];

    for (uint8_t low_index=0; low_index<m_n_trail_leds; low_index++)
    {
        m_pfn_value_getter(*this, low_index, multipliers, divisors, offsets);
        mp_values[low_index][0] = calculate_brightness(r, multipliers[0], divisors[0], offsets[0], ((1 << (8*sizeof(INT_TYPE)))-1));
        mp_values[low_index][1] = calculate_brightness(g, multipliers[1], divisors[1], offsets[1], ((1 << (8*sizeof(INT_TYPE)))-1));
        mp_values[low_index][2] = calculate_brightness(b, multipliers[2], divisors[2], offsets[2], ((1 << (8*sizeof(INT_TYPE)))-1));
    }

    if (m_running)
    {
        memset(mp_leds, 0, m_n_strip_leds*3);
        write_led_array();
        m_timer.start();
    }
}

template <class INT_TYPE>
void LEDTrail<INT_TYPE>::clear_last_pixel()
{
    if (m_tail_location_idx >= 0)
    {
        mp_leds[m_tail_location_idx][0] = 0;
        mp_leds[m_tail_location_idx][1] = 0;
        mp_leds[m_tail_location_idx][2] = 0;
    }
}

template <class INT_TYPE>
void LEDTrail<INT_TYPE>::set_next()
{
    if (m_tail_location_idx < m_n_strip_leds)
    {
        m_tail_location_idx++;
    }

    if (m_tail_location_idx == m_n_strip_leds)
    {
        if (m_pfn_complete_cb)
        {
            m_pfn_complete_cb(*this);
        }
    }
}

template <class INT_TYPE>
bool LEDTrail<INT_TYPE>::update()
{
    bool this_run_complete = false;
    if (m_running)
    {
        if (m_timer.check_and_restart())
        {
            this->clear_last_pixel();
            this->set_next();
            if (m_tail_location_idx == m_n_strip_leds)
            {
                this_run_complete = true;
                if (m_runs > 0)
                {
                    m_runs--;
                    m_running = m_runs > 0;
                }
            }
            write_led_array();
        }
    }
    else
    {
        if (m_timer.check_and_reset())
        {
            memset(mp_leds, 0, m_n_strip_leds*3);
        }
    }
    return !this_run_complete;
}

template <class INT_TYPE>
void LEDTrail<INT_TYPE>::write_led_array()
{
    int8_t write_index = m_tail_location_idx;
    for (int8_t i=0; i<m_n_trail_leds; i++)
    {
        // Only write to valid locations
        bool location_valid = true;
        location_valid &= (write_index + i) >= 0;
        location_valid &= (write_index + i) < m_n_strip_leds;
        if (location_valid)
        {
            //std::cout<<(int)(write_index+i)<<std::endl;
            memcpy(&mp_leds[(write_index+i)], &mp_values[i], 3);
        }
    }
}

#endif

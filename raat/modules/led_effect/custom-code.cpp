#include <Adafruit_NeoPixel.h>

#include "raat.hpp"

#include "adafruit-neopixel-raat.hpp"

#include "led-fader.hpp"
#include "larson-scanner.hpp"
#include "led-trail.hpp"

/* Defines, Constants, Typedefs */

typedef enum {
    Example_Fader,
    Example_Scanner,
    Example_Trail,
    Example_Merged
} Example;

/* LED Effect Callback Prototypes */

static void larson_value_provider(uint8_t index, uint8_t& r_multiplier, uint8_t& r_divisor, uint8_t& r_offset);
static void trail_value_provider(const LEDTrail<uint8_t>& r_trail, uint8_t index, uint8_t (&r_multipliers)[3], uint8_t (&r_divisors)[3], uint8_t (&r_offsets)[3]);

/* Private Data*/
static Example s_example = Example_Fader;
static uint8_t s_led_values[NLEDS][3] = {0};
static uint8_t s_led_values_2[NLEDS][3] = {0};
static LarsonScanner<uint8_t> s_larson = LarsonScanner<uint8_t>((uint8_t*)s_led_values, NLEDS, 5, 75, larson_value_provider);
static LEDTrail<uint8_t> s_led_trail = LEDTrail<uint8_t>(s_led_values, NLEDS, 5, 100, trail_value_provider, NULL);
static LEDTrail<uint8_t> s_led_trail_opposite = LEDTrail<uint8_t>(s_led_values_2, NLEDS, 5, 100, trail_value_provider, NULL);

static LEDFader<uint8_t> s_fader = LEDFader<uint8_t>(s_led_values, 1, LEDFaderType_SquaredAdjust);

/* LED Effect Callbacks */
static void larson_value_provider(uint8_t index, uint8_t& r_multiplier, uint8_t& r_divisor, uint8_t& r_offset)
{
    uint8_t multiplier = index+1;
    r_multiplier = multiplier * multiplier;
    r_divisor = ((NLEDS + 1) * (NLEDS + 1)) / 6;
    r_offset = (index + 1) * 5;
}

static void trail_value_provider(const LEDTrail<uint8_t>& r_trail, uint8_t index, uint8_t (&r_multipliers)[3], uint8_t (&r_divisors)[3], uint8_t (&r_offsets)[3])
{
    static const uint8_t offsets[2][3] = {
        {10,0,0}, // Offset for trail 1 in merged trail example
        {0,0,10}, // Offset for trail 2 in merged trail example
    };

    uint8_t multiplier = index+1;
    memset(r_multipliers, multiplier * multiplier, 3);
    memset(r_divisors, ((NLEDS + 1) * (NLEDS + 1)) / 6, 3);
    if (s_example == Example_Trail)
    {
        memset(r_offsets, 0, 3); // For the single trail example, use no offsets
    }
    else
    {
        // For the merged trails example, use a different offset depending on the trail
        const int offset_index = &r_trail == &s_led_trail ? 0 : 1;
        memcpy(r_offsets, &offsets[offset_index], 3);
    }
}

static void delay_between_sections(AdafruitNeoPixelRAAT * pNeoPixels, int delay1=75, int delay2=1000)
{
    delay(delay1);
    pNeoPixels->clear();
    pNeoPixels->show();
    memset(s_led_values, 0, NLEDS*3);
    delay(delay2);
}

static void led_fader_example(const raat_devices_struct& devices)
{
    /* Fade 5 random LEDs up and down, with random colours */
    for (uint8_t i = 0; i<5; i++)
    {
        uint8_t led_index = random(0, NLEDS);
        uint8_t rgb[3] = {
            (uint8_t)random(16,255), (uint8_t)random(16,255), (uint8_t)random(16,255)
        };

        // Fade Up
        s_fader.start(true, rgb[0], rgb[1], rgb[2], 255);
        while (s_fader.update())
        {
            devices.pNeoPixels->setPixelColor(led_index, s_led_values[0][0], s_led_values[0][1], s_led_values[0][2]);
            devices.pNeoPixels->show();
            delayMicroseconds(500);
        }

        // Fade Down
        s_fader.start(false, rgb[0], rgb[1], rgb[2], 255);
        while (s_fader.update())
        {
            devices.pNeoPixels->setPixelColor(led_index, s_led_values[0][0], s_led_values[0][1], s_led_values[0][2]);
            devices.pNeoPixels->show();
            delayMicroseconds(500);
        }

        delay_between_sections(devices.pNeoPixels, 0, 0);
    }
}

static void larson_scanner_example(const raat_devices_struct& devices)
{
    /* Run the Larson scanner 3 times, with a random colour */
    s_larson.start(random(16,255),random(16,255),random(16,255), 3);

    while (s_larson.update())
    {
        for (uint8_t i=0;i<NLEDS;i++)
        {
            devices.pNeoPixels->setPixelColor(i, s_led_values[i][0], s_led_values[i][1], s_led_values[i][2]);
        }
        devices.pNeoPixels->show();
    }
}

static void led_trail_example(const raat_devices_struct& devices)
{
    /* Run an LED trail from start to end, 3 times, with a random colour each time */
    for (uint8_t i = 0; i<3; i++)
    {
        s_led_trail.start(random(16,255), random(16,255), random(16,255));

        while (s_led_trail.update())
        {
            for (uint8_t i=0;i<NLEDS;i++)
            {
                devices.pNeoPixels->setPixelColor(i, s_led_values[i][0], s_led_values[i][1], s_led_values[i][2]);
            }
            devices.pNeoPixels->show();
        }

        delay_between_sections(devices.pNeoPixels, 75, 0);
    }
}

static void merged_trails_example(const raat_devices_struct& devices)
{
    /* Run LED trails (one red, one blue) from opposite ends of the string. Merge
    the colours in the middle by reversing one array and merging with the first */
    uint8_t led_values_2_reversed[NLEDS][3] = {0};
    uint8_t merged_values[NLEDS][3] = {0};

    for (uint8_t i = 0; i<3; i++)
    {
        s_led_trail.start(245, 0, 0);
        s_led_trail_opposite.start(0, 0, 245);

        while (s_led_trail.update() && s_led_trail_opposite.update())
        {
            reverse_copy<uint8_t>((uint8_t*)led_values_2_reversed, (uint8_t*)s_led_values_2, NLEDS, 3);
            led_effect_merge_arrays<uint8_t>(LEDEffectMergeType_Average, s_led_values, led_values_2_reversed, merged_values, NLEDS);

            for (uint8_t i=0;i<NLEDS;i++)
            {
                devices.pNeoPixels->setPixelColor(i, merged_values[i][0], merged_values[i][1], merged_values[i][2]);
            }
            devices.pNeoPixels->show();
        }

        delay_between_sections(devices.pNeoPixels, 75, 0);
    }
}

void raat_custom_setup(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices; (void)params;
    randomSeed(analogRead(0));
}

void raat_custom_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices; (void)params;

    s_example = Example_Fader;
    led_fader_example(devices);
    delay_between_sections(devices.pNeoPixels);

    s_example = Example_Scanner;
    larson_scanner_example(devices);
    delay_between_sections(devices.pNeoPixels);

    s_example = Example_Trail;
    led_trail_example(devices);
    delay_between_sections(devices.pNeoPixels);

    s_example = Example_Merged;
    merged_trails_example(devices);
    delay_between_sections(devices.pNeoPixels);
}

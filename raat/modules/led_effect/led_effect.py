import logging
from pathlib import Path

from yapsy.IPlugin import IPlugin

from raat.modules.generic_module import GenericModule

from raat.types import ModuleSource, ModuleInclude
from raat.modules.oneshot_timer.oneshot_timer import OneShotTimerModule

THIS_PATH = Path(__file__).parent


class LEDEffectModule(GenericModule):

    sources = OneShotTimerModule().sources

    includes = OneShotTimerModule().includes
    includes += (
        ModuleInclude(THIS_PATH, "led-effect.hpp"),
        ModuleInclude(THIS_PATH, "led-fader.hpp"),
        ModuleInclude(THIS_PATH, "larson-scanner.hpp"),
        ModuleInclude(THIS_PATH, "led-trail.hpp")
    )

    @property
    def directory(self):
        return THIS_PATH


class LEDEffectPlugin(IPlugin):
    def activate(self):
        pass

    def deactivate(self):
        pass

    def get(self, param):
        return LEDEffectModule()

    def set_log_level(self, level):
        logging.getLogger(__name__).setLevel(level)

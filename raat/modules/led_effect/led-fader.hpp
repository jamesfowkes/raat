#ifndef _RAAT_LED_FADER_H_
#define _RAAT_LED_FADER_H_

/* C/C++ Includes */

#include <stdint.h>

/* Module Includes */

#include "led-effect.hpp"

enum LEDFaderType
{
    LEDFaderType_Linear,
    LEDFaderType_SquaredAdjust,
    LEDFaderType_Custom
};

template<class INT_TYPE>
using setter_fn = void (*)(INT_TYPE (*pLEDS)[3], const uint8_t nleds, const INT_TYPE * m_p_max_values, const uint16_t stepnumber, const uint16_t max_stepnumber);

template <class INT_TYPE>
static void linearvalue_getter(INT_TYPE (*pLEDS)[3], const uint8_t nleds, const INT_TYPE * m_p_max_values, const uint16_t stepnumber, const uint16_t max_stepnumber)
{
    const INT_TYPE values[3] = {
        (INT_TYPE)((stepnumber * m_p_max_values[0])/max_stepnumber),
        (INT_TYPE)((stepnumber * m_p_max_values[1])/max_stepnumber),
        (INT_TYPE)((stepnumber * m_p_max_values[2])/max_stepnumber)
    };

    for (uint8_t i = 0; i < nleds; i++)
    {
        memcpy(pLEDS[i], values, sizeof(INT_TYPE)*3);
    }
}

template <class INT_TYPE>
static void squarevalue_getter(INT_TYPE (*pLEDS)[3], const uint8_t nleds, const INT_TYPE * m_p_max_values, const uint16_t stepnumber, const uint16_t max_stepnumber)
{
    const uint32_t multiplier = (uint32_t)stepnumber * (uint32_t)stepnumber;

    static uint16_t cached_max_stepnumber = 0;
    static uint32_t divisor = 0;

    if (max_stepnumber != cached_max_stepnumber)
    {
        cached_max_stepnumber = max_stepnumber;
        divisor = cached_max_stepnumber * cached_max_stepnumber;
    }

    for (uint8_t i = 0; i < nleds; i++)
    {
        pLEDS[i][0] = (multiplier * (uint32_t)m_p_max_values[0])/divisor;
        pLEDS[i][1] = (multiplier * (uint32_t)m_p_max_values[1])/divisor;
        pLEDS[i][2] = (multiplier * (uint32_t)m_p_max_values[2])/divisor;
    }
}


template <class INT_TYPE>
class LEDFader
{
public:
    LEDFader(INT_TYPE (*dst)[3], uint8_t n_leds, setter_fn<INT_TYPE>);
    LEDFader(INT_TYPE (*dst)[3], uint8_t n_leds, enum LEDFaderType faderType);

    void setup();
    void reset();

    void print();
    void print(INT_TYPE * pleds);
    void print(INT_TYPE * pleds, uint8_t min_index, uint8_t max_index);

    void start(bool direction_up, INT_TYPE max_r, INT_TYPE max_g, INT_TYPE max_b, uint16_t number_of_steps);
    void start(bool direction_up, INT_TYPE max_r, INT_TYPE max_g, INT_TYPE max_b, uint16_t number_of_steps, int8_t number_of_runs);

    bool update();

    uint16_t current_step() { return m_current_step; }
private:

    void set_next();

    INT_TYPE (*m_p_leds)[3];
    INT_TYPE * m_p_max_values;

    uint8_t m_n_leds;
    uint16_t m_current_step;
    uint16_t m_max_steps;
    bool m_direction_up;

    enum LEDFaderType m_fader_type;

    setter_fn<INT_TYPE> m_pfn_value_getter;
};

template <class INT_TYPE>
LEDFader<INT_TYPE>::LEDFader(INT_TYPE (*dst)[3], uint8_t n_leds, setter_fn<INT_TYPE> pfn_value_getter) :
    m_p_leds(dst), m_n_leds(n_leds), m_current_step(0), m_max_steps(0), m_direction_up(true),
    m_fader_type(LEDFaderType_Custom), m_pfn_value_getter(pfn_value_getter)
{
}

template <class INT_TYPE>
LEDFader<INT_TYPE>::LEDFader(INT_TYPE (*dst)[3], uint8_t n_leds, enum LEDFaderType faderType) :
    m_p_leds(dst), m_n_leds(n_leds), m_current_step(0), m_max_steps(0), m_direction_up(true),
    m_fader_type(faderType), m_pfn_value_getter(NULL)
{
    m_p_max_values = (INT_TYPE*)malloc(n_leds * 3 * sizeof(INT_TYPE));
    switch(m_fader_type)
    {
    case LEDFaderType_Linear:
        m_pfn_value_getter = linearvalue_getter;
        break;
    case LEDFaderType_SquaredAdjust:
        m_pfn_value_getter = squarevalue_getter;
        break;
    default:
    case LEDFaderType_Custom:
        break;
    }
}

template <class INT_TYPE>
void LEDFader<INT_TYPE>::start(bool direction_up, INT_TYPE max_r, INT_TYPE max_g, INT_TYPE max_b,
    uint16_t number_of_steps)
{
    m_direction_up = direction_up;
    m_current_step = 0;
    m_max_steps = number_of_steps;
    m_p_max_values[0] = max_r;
    m_p_max_values[1] = max_g;
    m_p_max_values[2] = max_b;
    if (direction_up)
    {
        memset(m_p_leds, 0, m_n_leds * sizeof(INT_TYPE) * 3);
    }
    else
    {
        for (uint8_t i = 0; i < m_n_leds; i++)
        {
            m_p_leds[i][0] = m_p_max_values[0];
            m_p_leds[i][1] = m_p_max_values[1];
            m_p_leds[i][2] = m_p_max_values[2];
        }
    }
}

template <class INT_TYPE>
bool LEDFader<INT_TYPE>:: update()
{
    bool last_step = m_current_step == m_max_steps;
    if (m_current_step <= m_max_steps)
    {
        uint16_t actual_step = m_direction_up ? m_current_step : m_max_steps - m_current_step;
        m_pfn_value_getter(m_p_leds, m_n_leds, m_p_max_values, actual_step, m_max_steps);
        m_current_step++;
    }
    return !last_step;
}

#endif

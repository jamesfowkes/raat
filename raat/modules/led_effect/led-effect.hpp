#ifndef _RAAT_LED_EFFECT_H_
#define _RAAT_LED_EFFECT_H_

/* RAAT Includes */

#include "raat-oneshot-timer.hpp"
#include "raat-util.hpp"

/* Defines, Constants, Typedefs */

typedef enum {
    LEDEffectMergeType_Average,
    LEDEffectMergeType_Max,
} LEDEffectMergeType;

typedef enum {
    LEDColour_R,
    LEDColour_G,
    LEDColour_B
} LEDColour;

static const int8_t CONTINUOUS_EFFECT = -1;

template <typename T>
void led_effect_merge_arrays(LEDEffectMergeType merge_type, T const (*p1)[3], T const (*p2)[3], T (*dst)[3], size_t n)
{
    if (p1 && p2 && dst)
    {
        for (size_t triplet=0; triplet<n; triplet++)
        {
            for (size_t i=0; i<3; i++)
            {
                // Need to use a type big enough to hold the result of any calculations.
                // Could use fancy templated type magic to generate the correct size types
                // but just using uint32 is much simpler
                const uint32_t val1 = p1[triplet][i];
                const uint32_t val2 = p2[triplet][i];
                switch(merge_type)
                {
                case LEDEffectMergeType_Average:
                    if (val1 && val2)
                    {
                        dst[triplet][i] = (val1+val2)/2;
                    }
                    else if (val1)
                    {
                        dst[triplet][i] = val1;
                    }
                    else if (val2)
                    {
                        dst[triplet][i] = val2;
                    }
                    else
                    {
                        dst[triplet][i] = 0;
                    }
                    break;
                case LEDEffectMergeType_Max:
                    dst[triplet][i] = val1 > val2 ? val1 : val2;
                    break;
                }
            }
        }
    }
}

#endif

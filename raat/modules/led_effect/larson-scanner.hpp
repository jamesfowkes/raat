#ifndef _RAAT_LARSON_SCANNER_H_
#define _RAAT_LARSON_SCANNER_H_

/* C/C++ Includes */

#include <stdint.h>

/* Module Includes */

#include "led-effect.hpp"

/* Defines, constants, typedefs */

enum direction
{
	DIR_POS,
	DIR_NEG
};

template <class INT_TYPE>
class LarsonScanner
{
public:
    LarsonScanner(INT_TYPE * dst, uint8_t n_strip_leds, uint8_t n_larson_leds, uint32_t update_period,
        void (*pfn_value_getter)(uint8_t index, INT_TYPE& r_multiplier, INT_TYPE& r_divisor, INT_TYPE& r_offset));

    void setup();
    void reset();
    void tick();

    void print();
    void print(INT_TYPE * pleds);
    void print(INT_TYPE * pleds, uint8_t min_index, uint8_t max_index);

    void start(INT_TYPE r, INT_TYPE g, INT_TYPE b);
    void start(INT_TYPE r, INT_TYPE g, INT_TYPE b, int8_t number_of_runs);

    bool update();
private:

	void set_next();
	void clear_last_pixel();
	RAATOneShotTimer m_timer;
	INT_TYPE * mp_leds;
	INT_TYPE * mp_values;
    uint8_t m_n_strip_leds;
    uint8_t m_n_larson_leds;
    uint8_t m_location_idx;
    uint8_t m_max_index;
    int8_t m_runs;
	bool m_running;
    enum direction m_direction;

    void (*m_pfn_value_getter)(uint8_t index, INT_TYPE& r_multiplier, INT_TYPE& r_divisor, INT_TYPE& r_offset);
};

template <class INT_TYPE>
LarsonScanner<INT_TYPE>::LarsonScanner(INT_TYPE * dst, uint8_t n_strip_leds, uint8_t n_larson_leds, uint32_t update_period,
    void (*pfn_value_getter)(uint8_t index, INT_TYPE& r_multiplier, INT_TYPE& r_divisor, INT_TYPE& r_offset)) :
	m_timer(update_period),
    mp_leds(dst), m_n_strip_leds(n_strip_leds), m_n_larson_leds(n_larson_leds),
    m_location_idx(0), m_max_index(n_strip_leds - n_larson_leds),
    m_pfn_value_getter(pfn_value_getter)
{
    mp_values = (INT_TYPE*)malloc(n_larson_leds * 3 * sizeof(INT_TYPE));
}

#ifndef ARDUINO
template <class INT_TYPE>
void LarsonScanner<INT_TYPE>::print()
{
    this->print(mp_leds);
}

template <class INT_TYPE>
void LarsonScanner<INT_TYPE>::print(INT_TYPE * pleds)
{
    uint8_t min_index = m_location_idx;
    if (min_index > 0)
    {
        min_index--;
    }

    uint8_t max_index = m_location_idx;

    if (max_index < m_n_strip_leds)
    {
        max_index++;
    }

    this->print(pleds, min_index, max_index);
}

template <class INT_TYPE>
void LarsonScanner<INT_TYPE>::print(INT_TYPE * pleds, uint8_t min_index, uint8_t max_index)
{
    std::cout << "LED values " << (int)min_index << "-" << (int)max_index << ":" << std::endl;
    for (uint8_t i=min_index; i<=max_index; i++)
    {
        std::cout << (int)pleds[i*3] << ","<< (int)pleds[i*3+1] << ","<< (int)pleds[i*3+2] << ",";
    }
    std::cout << std::endl;
}
#endif


template <class INT_TYPE>
void LarsonScanner<INT_TYPE>::start(INT_TYPE r, INT_TYPE g, INT_TYPE b)
{
    this->start(r, g, b, CONTINUOUS_EFFECT);
}

template <class INT_TYPE>
void LarsonScanner<INT_TYPE>::start(INT_TYPE r, INT_TYPE g, INT_TYPE b, int8_t number_of_runs)
{
    m_runs = number_of_runs;
	m_running = (number_of_runs > 0) || (number_of_runs == CONTINUOUS_EFFECT);
	m_location_idx = 0;

    const uint8_t middle_led_index = m_n_larson_leds/2;
    INT_TYPE divisor;
    INT_TYPE multiplier;
	INT_TYPE offset;

    for (uint8_t low_index=0; low_index<=middle_led_index; low_index++)
    {
		uint8_t high_index = m_n_larson_leds - low_index - 1;

        m_pfn_value_getter(low_index, multiplier, divisor, offset);
        mp_values[low_index*3] = ((r*multiplier)/divisor) + offset;
        mp_values[low_index*3+1] = ((g*multiplier)/divisor) + offset;
        mp_values[low_index*3+2] = ((b*multiplier)/divisor) + offset;

        mp_values[high_index*3] = ((r*multiplier)/divisor) + offset;
        mp_values[high_index*3+1] = ((g*multiplier)/divisor) + offset;
        mp_values[high_index*3+2] = ((b*multiplier)/divisor) + offset;
    }
    m_direction = DIR_POS;

	if (m_running)
	{
		INT_TYPE * pDst = &mp_leds[m_location_idx*3];
		memcpy(pDst, mp_values, m_n_larson_leds*3);
		m_timer.start();
	}
}

template <class INT_TYPE>
void LarsonScanner<INT_TYPE>::clear_last_pixel()
{
    uint16_t idx = 0xFFFF;
    if (m_direction == DIR_POS)
    {
        idx = m_location_idx;
    }
    else if (m_direction == DIR_NEG)
    {
        idx = m_location_idx+m_n_larson_leds-1;
    }

	if (idx != 0xFFFF)
	{
	    mp_leds[idx*3] = 0;
	    mp_leds[idx*3+1] = 0;
	    mp_leds[idx*3+2] = 0;
	}
}

template <class INT_TYPE>
void LarsonScanner<INT_TYPE>::set_next()
{
    if (m_direction == DIR_POS)
    {
        if (m_location_idx < m_max_index)
        {
            m_location_idx++;
        }

        if (m_location_idx == m_max_index)
        {
            m_direction = DIR_NEG;
        }
    }
    else
    {
        if (m_location_idx > 0)
        {
            m_location_idx--;
        }

        if (m_location_idx == 0)
        {
            m_direction = DIR_POS;
        }
    }
}

template <class INT_TYPE>
bool LarsonScanner<INT_TYPE>::update()
{
	bool still_running_frames = true;
	if (m_running)
	{
		if (m_timer.check_and_restart())
		{
		    this->clear_last_pixel();
		    this->set_next();
		    if (m_runs > 0)
		    {
		        if (m_location_idx == 0)
		        {
		            m_runs--;
		            m_running = m_runs > 0;
		        }
		    }

	        INT_TYPE * pDst = &mp_leds[m_location_idx*3];
	        memcpy(pDst, mp_values, m_n_larson_leds*3);
		}
	}
	else
	{
		if (m_timer.check_and_reset())
		{
			memset(mp_leds, 0, m_n_strip_leds*3);
			still_running_frames = false;
		}
	}
    return still_running_frames;
}


#endif

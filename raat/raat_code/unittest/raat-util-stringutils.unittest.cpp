#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include "raat-util.hpp"


class StringUtilsTest : public CppUnit::TestFixture {

    char output[32];

    CPPUNIT_TEST_SUITE(StringUtilsTest);

    CPPUNIT_TEST(testCopyAndTruncateStringCorrectlyCopiesFullStringWithLargerBuffer);
    CPPUNIT_TEST(testCopyAndTruncateStringCorrectlyCopiesFullStringWithSameSizeBuffer);
    CPPUNIT_TEST(testCopyAndTruncateStringCorrectlyCopiesPartialStringWithSmallerBuffer);
    CPPUNIT_TEST(testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize6);
    CPPUNIT_TEST(testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize5);
    CPPUNIT_TEST(testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize4);
    CPPUNIT_TEST(testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize3);
    CPPUNIT_TEST(testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize2);
    CPPUNIT_TEST(testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize1);
    CPPUNIT_TEST(testCopyAndTruncateStringDoesNothingWithBufferOfSize0);

    CPPUNIT_TEST_SUITE_END();

    void testCopyAndTruncateStringCorrectlyCopiesFullStringWithLargerBuffer()
    {
        // Setup
        const char input[] = "ABCDEFGHI";

        // Action
        size_t string_length = copy_and_truncate_with_ellipsis(output, input, 31);

        // Test
        CPPUNIT_ASSERT_EQUAL((size_t)9, string_length);
        CPPUNIT_ASSERT_EQUAL(std::string("ABCDEFGHI"), std::string(output));
    }

    void testCopyAndTruncateStringCorrectlyCopiesFullStringWithSameSizeBuffer()
    {
        // Setup
        const char input[] = "ABCDEFGHI";

        // Action
        size_t string_length = copy_and_truncate_with_ellipsis(output, input, 11);

        // Test
        CPPUNIT_ASSERT_EQUAL((size_t)9, string_length);
        CPPUNIT_ASSERT_EQUAL(std::string("ABCDEFGHI"), std::string(output));
    }

    void testCopyAndTruncateStringCorrectlyCopiesPartialStringWithSmallerBuffer()
    {
        // Setup
        const char input[] = "ABCDEFGHI";

        // Action
        size_t string_length = copy_and_truncate_with_ellipsis(output, input, 7);

        // Test
        CPPUNIT_ASSERT_EQUAL((size_t)6, string_length);
        CPPUNIT_ASSERT_EQUAL(std::string("AB...I"), std::string(output));
    }

    void testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize6()
    {
        // Setup
        const char input[] = "ABCDEFGHI";

        // Action
        size_t string_length = copy_and_truncate_with_ellipsis(output, input, 6);

        // Test
        CPPUNIT_ASSERT_EQUAL((size_t)5, string_length);
        CPPUNIT_ASSERT_EQUAL(std::string("A...I"), std::string(output));
    }

    void testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize5()
    {
        // Setup
        const char input[] = "ABCDEFGHI";

        // Action
        size_t string_length = copy_and_truncate_with_ellipsis(output, input, 5);

        // Test
        CPPUNIT_ASSERT_EQUAL((size_t)4, string_length);
        CPPUNIT_ASSERT_EQUAL(std::string("A..."), std::string(output));
    }

    void testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize4()
    {
        // Setup
        const char input[] = "ABCDEFGHI";

        // Action
        size_t string_length = copy_and_truncate_with_ellipsis(output, input, 4);

        // Test
        CPPUNIT_ASSERT_EQUAL((size_t)3, string_length);
        CPPUNIT_ASSERT_EQUAL(std::string("A.."), std::string(output));
    }

    void testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize3()
    {
        // Setup
        const char input[] = "ABCDEFGHI";

        // Action
        size_t string_length = copy_and_truncate_with_ellipsis(output, input, 3);

        // Test
        CPPUNIT_ASSERT_EQUAL((size_t)2, string_length);
        CPPUNIT_ASSERT_EQUAL(std::string("A."), std::string(output));
    }

    void testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize2()
    {
        // Setup
        const char input[] = "ABCDEFGHI";

        // Action
        size_t string_length = copy_and_truncate_with_ellipsis(output, input, 2);

        // Test
        CPPUNIT_ASSERT_EQUAL((size_t)1, string_length);
        CPPUNIT_ASSERT_EQUAL(std::string("A"), std::string(output));
    }

    void testCopyAndTruncateStringCorrectlyCopiesPartialStringWithBufferOfSize1()
    {
        // Setup
        const char input[] = "ABCDEFGHI";

        // Action
        size_t string_length = copy_and_truncate_with_ellipsis(output, input, 1);

        // Test
        CPPUNIT_ASSERT_EQUAL((size_t)0, string_length);
        CPPUNIT_ASSERT_EQUAL(std::string(""), std::string(output));
    }

    void testCopyAndTruncateStringDoesNothingWithBufferOfSize0()
    {
        // Setup
        const char input[] = "ABCDEFGHI";
        char expected_output[sizeof(output)];
        strcpy(expected_output, output);

        // Action
        size_t string_length = copy_and_truncate_with_ellipsis(output, input, 0);

        // Test
        CPPUNIT_ASSERT_EQUAL((size_t)0, string_length);
        CPPUNIT_ASSERT_EQUAL(std::string(expected_output), std::string(output));
    }

public:
    void setUp()
    {
        memset(output, 'X', sizeof(output));
        output[sizeof(output)-1] = '\0';
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(StringUtilsTest);

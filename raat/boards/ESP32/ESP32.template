/* ESP32 Includes */

#include <Esp.h>
#include <esp_system.h>
#include <EEPROM.h>
#include <WebOTA.h>

#include <WiFi.h>
#include <DNSServer.h>
#include <WebServer.h>
#include <WiFiManager.h>

/* RAAT Includes */

#include "raat.hpp"

{% for dep in board.raat_includes(False) %}
    #include "{{dep}}"
{% endfor %}

/* Local Constants */

const char* NTPSERVERS[] = {
    "pool.ntp.org",
    "0.pool.ntp.org",
    "1.pool.ntp.org"
};

/* Local Variables */

static WiFiManager s_wifiManager;

/* Local Functions */

static void onAccessPointCallback(WiFiManager * pManager)
{
    (void)pManager;
    Serial.print("Started access point ");
    Serial.print(s_wifiManager.getConfigPortalSSID());
    Serial.print(" at ");
    Serial.println(WiFi.softAPIP());
}

void app_wifi_setup()
{
    s_wifiManager.setDebugOutput(true);

    s_wifiManager.setAPCallback(onAccessPointCallback);

    s_wifiManager.autoConnect("{{board.captive_ssid}}");


    Serial.print("Connected to ");
    Serial.print(WiFi.SSID());
    Serial.print(" as ");
    Serial.println(WiFi.localIP());
}

void app_wifi_wipe_credentials()
{
    WiFi.disconnect(true);
    WiFi.begin("0","0");
}

/* RAAT Functions */

int raat_board_strcpy_progmem(char * const pMemoryDst, char const * const pProgmemSrc)
{
    return(strlen(strcpy(pMemoryDst, pProgmemSrc)));
}

int raat_board_memcmp_progmem(char * const pMemory, char const * const pProgmem, size_t len)
{
    return memcmp(pMemory, pProgmem, len);
}

int raat_board_strcmp_progmem(char * const pMemory, char const * const pProgmem)
{
    return strcmp(pMemory, pProgmem);
}

int raat_board_strncmp_progmem(char const * const pMemory, char const * const pProgmem, size_t len)
{
    return strncmp(pMemory, pProgmem, len);
}

int raat_board_strlen_progmem(char const * const pProgmem)
{
	return strlen(pProgmem);
}

void raat_board_memcpy_progmem(char * pMemory, char const * const pProgmem, size_t len)
{
    memcpy(pMemory, pProgmem, len);
}

uint16_t raat_board_max_adc_reading()
{
	return 4095U;
}

void raat_board_setup()
{
    Serial.setDebugOutput(true);
    esp_log_level_set("*", ESP_LOG_VERBOSE);

    app_wifi_setup();

    configTzTime(PSTR("GMT0BST-1,M3.5.0/1,M10.5.0"), NTPSERVERS[0],  NTPSERVERS[1],  NTPSERVERS[2]);
    webota.set_custom_html("<h1>{{board.name}} OTA Update</h1>");
}

void raat_board_loop()
{
	webota.handle();
}

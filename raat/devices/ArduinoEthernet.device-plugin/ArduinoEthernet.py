import logging

from pathlib import Path

from collections import namedtuple

from yapsy.IPlugin import IPlugin

from raat.types import DeviceSource, DeviceInclude, LibraryInclude, ModuleSource, ModuleInclude

from raat.devices.generic_device import GenericDevice, GenericDevicePlugin

THIS_PATH = Path(__file__).parent
MODULES_PATH = Path(THIS_PATH.parent.parent, "modules")
BUFFER_PATH = Path(MODULES_PATH, "buffer")

class ArduinoEthernetRAAT(GenericDevice, namedtuple("ArduinoEthernetRAAT", ["name", "cs_pin"])):

    __slots__ = ()

    sources = (DeviceSource(THIS_PATH, "ArduinoEthernetRAAT.cpp"), ModuleSource(BUFFER_PATH, "raat-buffer.cpp"))

    includes = (
        LibraryInclude("Ethernet.h"),
        DeviceInclude(THIS_PATH, "ArduinoEthernetRAAT.hpp"),
        ModuleInclude(BUFFER_PATH, "raat-buffer.hpp")
    )

    @property
    def required_libraries(self):
        return ["Ethernet"]

    @property
    def setup(self):
        return "{name}.setup();".format(name=self.cname())

    @property
    def command_handler(self):
        return "return {name}.command_handler(command, reply, max_length);".format(name=self.cname())

    @property
    def directory(self):
        return THIS_PATH

    @property
    def declarations(self):
        return "static ArduinoEthernetRAAT {name} = ArduinoEthernetRAAT({cs_pin});".format(name=self.cname(), cs_pin=self.cs_pin)


class ArduinoEthernetPlugin(IPlugin, GenericDevicePlugin):

    REQUIRED_SETTINGS = ["cs_pin"]

    device_class = ArduinoEthernetRAAT

    def activate(self):
        pass

    def deactivate(self):
        pass

    def get(self, device):
        self.verify_settings(device)

        return ArduinoEthernetRAAT(device.name, device.settings["cs_pin"].value)

    def set_log_level(self, level):
        logging.getLogger(__name__).setLevel(level)

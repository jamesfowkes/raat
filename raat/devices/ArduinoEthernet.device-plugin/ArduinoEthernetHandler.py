""" ArduinoEthernetHandler.py

Usage:
    ArduinoEthernetHandler.py <port> <device_number> [--baud=<baud>] [--ip=<ip>] [--mac=<mac>] [--gateway=<gateway>] [--prot=<prot>][--nowait]

Options:
    --nowait  Do not wait to receive lines from the device

"""

import serial
import docopt
import random

def read_until_condition(ser, condition, printer=None):
    while True:
        l = ser.readline()
        if (printer):
            printer(l)
        if condition(l):
            break;

def line_printer(l):
    if len(l) > 0:
        print(l.decode("utf-8").strip())

def random_mac():
    return "{:02X}:{:02X}:{:02X}:{:02X}:{:02X}:{:02X}".format(
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255)
    )

def write_command(ser, cmd):
    ser.write((cmd + "\r\n").encode("utf-8"))
    read_until_condition(ser, lambda l: len(l) > 0, line_printer)

def get_protocol_cmd(cmd, device_number, ip, protocol):
    if protocol == "url":
        return "/device/{:02d}/{}{}".format(device_number, cmd, ip)
    elif protocol == "simple":
        return "D{:02d}{}{}".format(device_number, cmd, ip)

def get_ip_cmd(device_number, ip, protocol):
    return get_protocol_cmd("SIP", device_number, ip, protocol)

def get_mac_cmd(device_number, mac, protocol):
    return get_protocol_cmd("SMAC", device_number, mac, protocol)

def get_gateway_cmd(device_number, gateway, protocol):
    return get_protocol_cmd("SGWAY", device_number, gateway, protocol)

if __name__ == "__main__":

    args = docopt.docopt(__doc__)

    print(args)
    baudrate = args["--baud"] or 115200
    device_number = int(args["<device_number>"])
    protocol = args.get("--prot", "url")

    with serial.Serial(port=args["<port>"], baudrate=baudrate, timeout=1) as ser:
        if not args["--nowait"] :
            read_until_condition(ser, lambda l: len(l) > 0, line_printer)
            read_until_condition(ser, lambda l: len(l) == 0, line_printer)

        if args["--ip"] is not None:
            cmd = get_ip_cmd(device_number, args["--ip"], protocol)
            print(cmd)
            write_command(ser, cmd)

        if args["--mac"] is not None:
            if args["--mac"] == "?":
                args["--mac"] = random_mac()

            cmd = get_mac_cmd(device_number, args["--mac"], protocol)
            print(cmd)
            write_command(ser, cmd)

        if args["--gateway"] is not None:
            cmd = get_gateway_cmd(device_number, args["--gateway"], protocol)
            print(cmd)
            write_command(ser, cmd)

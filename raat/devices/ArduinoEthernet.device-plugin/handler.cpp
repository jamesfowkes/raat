#include <stdint.h>
#include <string.h>

#include "raat.hpp"
#include "raat-buffer.hpp"

#include "http-get-server.hpp"

static HTTPGetServer s_server(NULL);

static void send_standard_erm_response()
{
    s_server.set_response_code("200 OK");
    s_server.set_header("Access-Control-Allow-Origin", "*");
    s_server.finish_headers();
}

static void test1_handler(HTTPGetServer& server, char const * const url, char const * const additional)
{
    s_server.set_response_code("200 OK");
    s_server.set_header("Access-Control-Allow-Origin", "*");
    s_server.finish_headers();
    s_server.add_body("<html><body><h1>Test 1</body></h1></html>\r\n");
}

static void test2_handler(HTTPGetServer& server, char const * const url, char const * const additional)
{
    s_server.set_response_code("200 OK");
    s_server.set_header("Access-Control-Allow-Origin", "*");
    s_server.finish_headers();
    s_server.add_body("<html><body><h1>Test 2</body></h1></html>\r\n");
}

static const char TEST1_URL[] PROGMEM = "/test/1";
static const char TEST2_URL[] PROGMEM = "/test/2";

static http_get_handler s_handlers[] = 
{
    {TEST1_URL, test1_handler},
    {TEST2_URL, test2_handler},
    {"", NULL}
};

void ethernet_packet_handler(char * req)
{
	s_server.handle_req(s_handlers, req);
}

char * ethernet_response_provider()
{
	return s_server.get_response();
}

void raat_custom_setup(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices; (void)params;
    pinMode(13, OUTPUT);
}

void raat_custom_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices; (void)params;
}

#include <SPI.h>
#include <Ethernet.h>

#include "raat.hpp"

#include "ArduinoEthernetRAAT.hpp"

static const char IP_FORMAT[] PROGRAM_MEMORY = "IP: %d.%d.%d.%d";
static const char MAC_FORMAT[] PROGRAM_MEMORY = "MAC: %02x:%02x:%02x:%02x:%02x:%02x";

static const char INIT_STRING[] PROGRAM_MEMORY = "Initialising ArduinoEthernet device...";
static const char CONFIG_STRING[] PROGRAM_MEMORY = "Configuring network params...";
static const char FAIL_STRING[] PROGRAM_MEMORY = "Failed to access ArduinoEthernet device!";
static const char LINK_OFF_STRING[] PROGRAM_MEMORY = "Check cable connection!";
static const char STARTING_STRING[] PROGRAM_MEMORY = "Starting server...";

ArduinoEthernetRAAT::ArduinoEthernetRAAT(uint8_t cs_pin) :
    m_cs_pin(cs_pin), m_server(80), m_buffer(m_bufferchars, ARDUINO_ETHERNET_BUFFER_SIZE)
{
    m_mac_eeprom_location.size = 6;
    m_ip_eeprom_location.size = 4;
    raat_nv_alloc(m_mac_eeprom_location);
    raat_nv_alloc(m_ip_eeprom_location);
}

void ArduinoEthernetRAAT::tick()
{
    bool currentLineIsBlank = false;
    EthernetClient client = m_server.available();
    if (client)
    {
        while (client.connected())
        {
            if (client.available())
            {
                char c = client.read();
                Serial.write(c);
                m_buffer.writeChar(c);
                if (c == '\n' && currentLineIsBlank)
                {
                    ethernet_packet_handler(m_bufferchars);
                    client.print(ethernet_response_provider());
                    m_buffer.reset();
                    break;
                }

                if (c == '\n') {
                  // you're starting a new line
                  currentLineIsBlank = true;
                } else if (c != '\r') {
                  // you've gotten a character on the current line
                  currentLineIsBlank = false;
                }
            }
        }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
}

void ArduinoEthernetRAAT::reset()
{

}

void ArduinoEthernetRAAT::print_settings(RAAT_LOG_MODULES log_module)
{
    raat_logln_P(log_module, IP_FORMAT,
        m_ip_address[0], m_ip_address[1], m_ip_address[2], m_ip_address[3]);
    raat_logln_P(log_module, MAC_FORMAT,
        m_mac_address[0], m_mac_address[1], m_mac_address[2], m_mac_address[3], m_mac_address[4], m_mac_address[5]);
}

void ArduinoEthernetRAAT::setup()
{
    this->reset();

    raat_nv_load(m_mac_address, m_mac_eeprom_location);
    raat_nv_load(m_ip_address, m_ip_eeprom_location);

    #ifdef ARDUINO_ARCH_AVR
    if (m_cs_pin != 10)
    {
        raat_logln_P(LOG_RAAT, PSTR("Warning: ensure pin 10 is correctly set!"));
    }
    #endif

    Ethernet.init(m_cs_pin);

    this->print_settings(LOG_RAAT);

    raat_logln_P(LOG_RAAT, INIT_STRING);
    raat_logln_P(LOG_RAAT, CONFIG_STRING);
    Ethernet.begin(m_mac_address, m_ip_address);

    if (Ethernet.hardwareStatus() == EthernetNoHardware)
    {
        raat_logln_P(LOG_RAAT, FAIL_STRING);
        while (true) {
        delay(1); // do nothing, no point running without Ethernet hardware
        }
    }
    if (Ethernet.linkStatus() == LinkOFF)
    {
        raat_logln_P(LOG_RAAT, LINK_OFF_STRING);
    }

    raat_logln_P(LOG_RAAT, STARTING_STRING);

    m_server.begin();
}

int ArduinoEthernetRAAT::handle_set_command(char const * const command, char * reply)
{
    enum
    {
        BAD_COMMAND,
        BAD_VALUE,
        OK
    } result = BAD_COMMAND;

    int n = 0;
    unsigned int local_scan_buffer[6] = {0};
    uint8_t reply_length = 0;

    if (strncmp_P(command, PSTR("MAC"), 3) == 0)
    {
        n = sscanf_P(command+3, PSTR("%02x:%02x:%02x:%02x:%02x:%02x"),
            &local_scan_buffer[0], &local_scan_buffer[1], &local_scan_buffer[2],
            &local_scan_buffer[3], &local_scan_buffer[4], &local_scan_buffer[5]
        );
        result = (n == 6) ? OK : BAD_VALUE;
        if (result == OK)
        {
            for (uint8_t i=0; i<6; i++) { m_mac_address[i] = local_scan_buffer[i]; }
            raat_nv_save(m_mac_address, m_mac_eeprom_location);
        }
    }
    else if (strncmp_P(command, PSTR("IP"), 2) == 0)
    {
        n = sscanf_P(command+2, PSTR("%d.%d.%d.%d"),
            &local_scan_buffer[0], &local_scan_buffer[1], &local_scan_buffer[2], &local_scan_buffer[3]
        );
        result = (n == 4) ? OK : BAD_VALUE;
        if (result == OK)
        {
            for (uint8_t i=0; i<4; i++) { m_ip_address[i] = local_scan_buffer[i]; }
            raat_nv_save(m_ip_address, m_ip_eeprom_location);
        }
    }

    switch(result)
    {
    case OK:
        strncpy_P(reply, PSTR("OK"), 2);
        reply_length = 2;
        break;
    case BAD_VALUE:
        reply_length = sprintf_P(reply, PSTR("VAL (n=%d)?"), n);
        break;
    case BAD_COMMAND:
        strncpy_P(reply, PSTR("CMD?"), 4);
        reply_length = 4;
        break;
    }


    return reply_length;
}

uint16_t ArduinoEthernetRAAT::command_handler(char const * const command, char * reply, uint16_t max_length)
{
    (void)max_length;
    int reply_length = 0;

    switch(command[0])
    {
    case 'S':
        reply_length = this->handle_set_command(&command[1], reply);
        break;
    case '?':
        raat_logging_set_special_prefix(PSTR("ETH"));
        this->print_settings(LOG_SPECIAL);
        strncpy_P(reply, PSTR("OK"), 2);
        reply_length = 2;
        break;
    }
    return reply_length;
}

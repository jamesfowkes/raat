#ifndef _ARDUINOETHERNETRAAT_H_
#define _ARDUINOETHERNETRAAT_H_

#include "raat-buffer.hpp"

class ArduinoEthernetRAAT : public DeviceBase
{
public:
    ArduinoEthernetRAAT(uint8_t cs_pin);
    void setup();
    void reset();
    void tick();
    uint16_t command_handler(char const * const command, char * reply, uint16_t max_length);
    
    void print_settings(RAAT_LOG_MODULES log_module);

private:

	int handle_set_command(char const * const command, char * reply);

	uint8_t m_mac_address[6];
	uint8_t m_ip_address[4];

    uint8_t m_cs_pin;
    
    RAAT_NV_LOCATION m_mac_eeprom_location;
    RAAT_NV_LOCATION m_ip_eeprom_location;

    EthernetServer m_server;
    char m_bufferchars[ARDUINO_ETHERNET_BUFFER_SIZE];
    RAATBuffer m_buffer;
};

// Expected to be implemented by userspace code
void ethernet_packet_handler(char * req);
char * ethernet_response_provider();

#endif

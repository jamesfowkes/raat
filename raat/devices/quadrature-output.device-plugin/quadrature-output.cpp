#include "raat.hpp"

#include "quadrature-output.hpp"

#define PERIOD_MS (2000)
#define STEP_TIME_MS (PERIOD_MS / 4)

QuadratureOutput::QuadratureOutput(uint8_t pin1, uint8_t pin2) :
    m_pin1(pin1), m_pin2(pin2), m_step(0), m_tick(STEP_TIME_MS), m_dir(QuadratureOutputDir_Fwd), m_run(false)
{
}

void QuadratureOutput::reset()
{
    digitalWrite(m_pin1, LOW);
    digitalWrite(m_pin2, LOW);
    m_tick = STEP_TIME_MS;
    m_step = 0;
    m_dir = QuadratureOutputDir_Fwd;
    m_run = false;
}

void QuadratureOutput::setup()
{
    this->reset();
    pinMode(m_pin1, OUTPUT);
    pinMode(m_pin2, OUTPUT);
}

uint16_t QuadratureOutput::command_handler(char const * const command, char * reply, uint16_t max_length)
{
    (void)max_length;
    int reply_length = 0;

    if (command[0] == 'R')
    {
        this->reset();
        strcpy(reply, "ROK");
        reply_length = strlen(reply);
    }
    else if (command[0] == '1')
    {
        this->enable(true);
        strcpy(reply, "1OK");
        reply_length = strlen(reply);
    }
    else if (command[0] == '0')
    {
        this->enable(false);
        strcpy(reply, "0OK");
        reply_length = strlen(reply);
    }
    else if (command[0] == 'F')
    {
        this->set_direction(QuadratureOutputDir_Fwd);
        strcpy(reply, "FOK");
        reply_length = strlen(reply);
    }
    else if (command[0] == 'B')
    {
        this->set_direction(QuadratureOutputDir_Back);
        strcpy(reply, "BOK");
        reply_length = strlen(reply);
    }
    else
    {
        reply[0] = '?';
        reply_length = 1;
    }

    return reply_length;
}

void QuadratureOutput::tick(void)
{
    if (m_run)
    {
        if (m_tick < RAAT_TICK_MS)
        {
            this->step();
            m_tick = STEP_TIME_MS;
        }
        else
        {
            m_tick -= RAAT_TICK_MS;
        }
    }
}

void QuadratureOutput::step(void)
{
    m_step += (m_dir == QuadratureOutputDir_Fwd ? 1 : -1);
    switch (m_step & 0x03)
    {
    case 0:
        // Pin1 stays low
        digitalWrite(m_pin2, LOW);
        break;
    case 1:
        digitalWrite(m_pin1, HIGH);
        // Pin2 stays low
        break;
    case 2:
        // Pin1 stays HIGH
        digitalWrite(m_pin2, HIGH);
        break;
    case 3:
        digitalWrite(m_pin1, LOW);
        // Pin2 stays high
        break;
    }
    
}

import logging

from pathlib import Path

from collections import namedtuple

from yapsy.IPlugin import IPlugin

from raat.types import DeviceSource, DeviceInclude, ModuleSource, ModuleInclude

from raat.devices.generic_device import GenericDevice, GenericDevicePlugin

from raat.types import Setting

THIS_PATH = Path(__file__).parent
MODULES_PATH = Path(THIS_PATH.parent.parent, "modules")
ONESHOT_TIMER_PATH = Path(MODULES_PATH, "oneshot_timer")


class QuadratureOutput(GenericDevice, namedtuple("QuadratureOutput", ["name", "pin1", "pin2"])):

    __slots__ = ()

    sources = (DeviceSource(THIS_PATH, "quadrature-output.cpp"), )
    includes = (DeviceInclude(THIS_PATH, "quadrature-output.hpp"), )

    @property
    def setup(self):
        return "{name}.setup();".format(name=self.cname())

    @property
    def command_handler(self):
        return "return {name}.command_handler(command, reply, max_length);".format(name=self.cname())

    @property
    def directory(self):
        return THIS_PATH

    @property
    def declarations(self):
        return "static QuadratureOutput {name} = QuadratureOutput({pin1}, {pin2});".format(
            name=self.cname(), pin1=self.pin1.value, pin2=self.pin2.value
        )


class QuadratureOutputPlugin(IPlugin, GenericDevicePlugin):

    REQUIRED_SETTINGS = ["pin1", "pin2"]

    device_class = QuadratureOutput

    def activate(self):
        pass

    def deactivate(self):
        pass

    def get(self, device):

        self.verify_settings(device)

        return QuadratureOutput(device.name, device.settings["pin1"], device.settings["pin2"])

    def set_log_level(self, level):
        logging.getLogger(__name__).setLevel(level)

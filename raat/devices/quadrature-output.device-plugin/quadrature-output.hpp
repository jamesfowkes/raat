#ifndef _QUADRATURE_OUTPUT_H_
#define _QUADRATURE_OUTPUT_H_

typedef enum {
    QuadratureOutputDir_Fwd,
    QuadratureOutputDir_Back
} QuadratureOutputDir;

class QuadratureOutput : public DeviceBase
{
public:
    QuadratureOutput(uint8_t pin1, uint8_t pin2);
    void setup();
    void reset();
    void tick();
    uint16_t command_handler(char const * const command, char * reply, uint16_t max_length);
    void enable(bool en) { m_run = en; }
    void set_direction(QuadratureOutputDir dir) { m_dir = dir; }
private:

    void step(void);

    uint8_t m_pin1;
    uint8_t m_pin2;
    uint8_t m_step;
    uint16_t m_tick;
    QuadratureOutputDir m_dir;
    bool m_run;
};

#endif

#include <Arduino.h>

#include <PinChangeInterrupt.h>

#include "raat.hpp"

#include "rotary-encoder.hpp"

RotaryEncoder::RotaryEncoder(uint8_t chA, uint8_t chB) : m_chA(chA), m_chB(chB), m_count(0)
{
}

void RotaryEncoder::reset() {}

void RotaryEncoder::setup()
{
    pinMode(this->m_chA, INPUT_PULLUP);
    pinMode(this->m_chB, INPUT_PULLUP);

    m_a_state = digitalRead(this->m_chA) == HIGH;
    m_b_state = digitalRead(this->m_chB) == HIGH;
}

void RotaryEncoder::setInterrupts(void(*ChannelAInterrupt)(), void (*ChannelBInterrupt)())
{
    attachPCINT(digitalPinToPCINT(this->m_chA), ChannelAInterrupt, CHANGE);
    attachPCINT(digitalPinToPCINT(this->m_chB), ChannelBInterrupt, CHANGE);
}

uint16_t RotaryEncoder::command_handler(char const * const command, char * reply, uint16_t max_length)
{
    int reply_len = 0;

    strcpy(reply, "CMD?");
    reply_len = 4;
    return reply_len;
}

void RotaryEncoder::tick()
{

}

int32_t RotaryEncoder::count()
{
    return m_count;
}

void RotaryEncoder::ChannelAInterrupt()
{ 
    bool old_a_state = m_a_state;
    m_a_state = digitalRead(this->m_chA) == HIGH;

    if (old_a_state && !m_a_state && m_b_state)
    {
        m_count++; // HIGH-to-LOW, B HIGH
    }
    else if (old_a_state && !m_a_state && !m_b_state)
    {
        m_count--; // HIGH-to-LOW, B LOW
    }
    else if (!old_a_state && m_a_state && m_b_state)
    {
        m_count--; // LOW-to-HIGH, B HIGH
    }
    else if (!old_a_state && m_a_state && !m_b_state)
    {
        m_count++; // LOW-to-HIGH, B LOW
    }
}

void RotaryEncoder::ChannelBInterrupt()
{ 
    bool old_b_state = m_b_state;
    m_b_state = digitalRead(this->m_chB) == HIGH;

    if (old_b_state && !old_b_state && m_a_state)
    {
        m_count--; // HIGH-to-LOW, A HIGH
    }
    else if (old_b_state && !old_b_state && !m_a_state)
    {
        m_count++; // HIGH-to-LOW, A LOW
    }
    else if (!old_b_state && old_b_state && m_a_state)
    {
        m_count++; // LOW-to-HIGH, A HIGH
    }
    else if (!old_b_state && old_b_state && !m_a_state)
    {
        m_count--; // LOW-to-HIGH, A LOW
    }
}


import logging

from pathlib import Path

from collections import namedtuple

from yapsy.IPlugin import IPlugin

from raat.types import DeviceSource, DeviceInclude, LibraryInclude

from raat.devices.generic_device import GenericDevice, GenericDevicePlugin

THIS_PATH = Path(__file__).parent


class RotaryEncoder(GenericDevice, namedtuple("RotaryEncoder", ["name", "chA", "chB"])):

    __slots__ = ()

    sources = (DeviceSource(THIS_PATH, "rotary-encoder.cpp"), )

    includes = (
        LibraryInclude("PinChangeInterrupt.h"),
        DeviceInclude(THIS_PATH, "rotary-encoder.hpp")
    )

    @property
    def required_libraries(self):
        return ["PinChangeInterrupt"]

    @property
    def setup(self):
        return """
    {name}.setup();
    {name}.setInterrupts(Encoder_{name}_ChannelAInterrupt, Encoder_{name}_ChannelBInterrupt);""".format(name=self.cname())

    @property
    def command_handler(self):
        return "return {name}.command_handler(command, reply, max_length);".format(name=self.cname())

    @property
    def directory(self):
        return THIS_PATH

    @property
    def declarations(self):
        return """
static RotaryEncoder {name} = RotaryEncoder({chA}, {chB});
void Encoder_{name}_ChannelAInterrupt()
{{
    {name}.ChannelAInterrupt();
}}
void Encoder_{name}_ChannelBInterrupt()
{{
    {name}.ChannelBInterrupt();   
}}
""".format(name=self.cname(), chA=self.chA, chB=self.chB)


class RotaryEncoderPlugin(IPlugin, GenericDevicePlugin):

    REQUIRED_SETTINGS = ["chA", "chB"]

    device_class = RotaryEncoder

    def activate(self):
        pass

    def deactivate(self):
        pass

    def get(self, device):
        self.verify_settings(device)

        return RotaryEncoder(device.name, device.settings["chA"].value, device.settings["chB"].value)

    def set_log_level(self, level):
        logging.getLogger(__name__).setLevel(level)

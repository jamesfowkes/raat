#ifndef _RAAT_ROTARY_ENCODER_H_
#define _RAAT_ROTARY_ENCODER_H_

class RotaryEncoder : public DeviceBase
{
public:
    RotaryEncoder(uint8_t chA, uint8_t chB);
    void reset();
    void setup();
    void setInterrupts(void (*ChannelAInterrupt)(), void (*ChannelBInterrupt)());
    uint16_t command_handler(char const * const command, char * reply, uint16_t max_length);
    void tick();

    void ChannelAInterrupt();
    void ChannelBInterrupt();

    int32_t count();

private:
    uint8_t m_chA;
    uint8_t m_chB;

    bool m_a_state;
    bool m_b_state;

    int32_t m_count;
};

#endif
